Gmap ArcGIS JS
==============

This small library helps you to manage lots of ArcGIS layers and markers in your Google Maps v3.

Features
--------

  - Can easily show and hide KMLs, layers and markers from any ArcGIS server.
  - Support for Identify tasks.
  - jQuery slider support to show multiple layers using a scrollable slider.

Requirements
------------

  - ArcGIS Link.
  - jQuery and jQuery UI.

Usage
-----

Create a gmapArcgis object for your map and define layers, markers and kmls:

    var myGmapArcgis = new gmapArcgis({
      // Map object
      map: map,
    
      // Map html element
      mapName: mapID,
    
      layers: {
        mylayer: {
          overlayTime: 5000,
          uri:         mapServiceUrl,
          opacity:     0.65,
          layers:      [1],
          showOn:      [ 'map' ],
        },
    
      markers: {
        mymarker: {
          uri:         mapServiceUrl + '/' + layerName,
          overlayTime: 2000,
          fields:      [ ],
          content:     markerCallback,
          icon:        'example.png',
          showOn:      [ 'map' ],
        },
      },
    });

Then you should be able to display them easily:

    myGmapArcgis.addLayers('mylayer');
    myGmapArcgis.addMarkers('mymarker');

Examples
--------

Check the examples/ folder for working examples.
