// Map element
var mapID = 'map';

// Service URL
var mapService = 'TODO';

// Set map options
var mapOptions = {
  center:      new google.maps.LatLng(0, 0),
  zoom:        5,
  scrollwheel: false,
  mapTypeId:   google.maps.MapTypeId.TERRAIN,
};

// Create a map instance
var map = new google.maps.Map(document.getElementById(mapID), mapOptions);

// Setup a gmapArcgis wrapper
var myGmapArcgis = new gmapArcgis({
  map: map,

  mapName: mapID,

  layers: {
    mylayer: {
      overlayTime:     5000,
      uri:             mapService,
      opacity:         0.65,
      layers:          [1],
      showOn:          [ 'map' ],
    },

  markers: {
    mymarker: {
      uri:         mapService + '/1',
      overlayTime: 2000,
      fields:      [ ],
      content:     markerCallback,
      icon:        'example.png',
      showOn:      [ 'map' ],
    },
  },
});

myGmapArcgis.addLayers('mylayer');
myGmapArcgis.addMarkers('mymarker');

function markerCallback(attributes) {
  return attributes.join(' ');
}
